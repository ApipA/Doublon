#!/usr/bin/python
# -*- coding: utf-8 -*

### IMPORTS

import sys
import hashlib
import os
import datetime
import csv
import codecs
import time
from ast import literal_eval

### end IMPORTS

### VARS

#Directory to scan
SCAN_DIR = (r"D:\Dev\script\Doublon de fichiers\arborescence")
FILELIST = (r"fileList.csv")
### end VARS

### FUNCTIONS

#Fonction de calcul de hash MD5
def md5sum(filename, block_size=65536):
    md5sum = hashlib.md5()
    with open(filename, 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            md5sum.update(block)
    return md5sum.hexdigest()

#Fonction de recherche de doublon
def doublon(line):
    #doublon = MD5LISTE.count(MD5LISTE[0])
    #print(doublon)

    line = tuple(s for s in line.split(','))

    for hash in line[0]:
        print(hash)
        #LISTE[3].remove(hash)
        #if hash in LISTE:
        #    print("doublon : ", hash)
        #else:
        #    print("pas de doublon")



### end FUNCTIONS

#Inserer les elements dans une liste : Nom, chemin, taille, hash MD5
LISTE=[]
MD5LISTE=[]
for path, dirs, files in os.walk(SCAN_DIR, followlinks=False):
    for filename in files:

        #Concatenation du chemin + nom du fichier pour obtenir le chemin absolu
        FILE = os.path.join(path, filename)

        #Date de derniere modification
        mtimestamp = os.path.getmtime(FILE)
        mtime = datetime.datetime.fromtimestamp(mtimestamp)

        #Taille du fichier
        size = os.path.getsize(FILE)

        #Appel de la fonction de calcul du hash MD5
        md5 = md5sum(FILE)
        MD5LISTE.append(md5)
        #Mise a jour de la liste
        item = [filename, path, size, md5]
        LISTE.append(item)
        #print (item)
        #print (LISTE)
#print(MD5LISTE)

nbFile = len(MD5LISTE)
print(nbFile, "fichiers")

#doublon(LISTE)

f = open(FILELIST, 'r', encoding="utf-8", errors="ignore")
line = f.readline()
COUNTER = 100000
cpt = 0
ts = time.time()
while line:
    doublon(line)
    cpt += 1
    if cpt == COUNTER :
        cpt = 0
        old_ts = ts
        ts = time.time()
        delay = ts - old_ts
        print (int(COUNTER/delay/1000),' Mille / Seconde')
    line = f.readline()
f.close()



#crée le fichier csv et écrit dedans)
with open('fileList.csv', 'w', newline='') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerows(LISTE)
