#!/usr/bin/python
# -- coding: utf-8 -
import hashlib
import os
import shutil

# Vars
SCAN_DIR = (r"C:\Users\Samantha.bizineche\Desktop\Test")
MD5 = []
DOUBLON = []

#########################""

def md5sum(filename, block_size=65536):
    md5sum = hashlib.md5()
    with open(filename, 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            md5sum.update(block)
    return md5sum.hexdigest()

for path, dirs, files in os.walk(SCAN_DIR, followlinks=False):
    for filename in files:
        print (filename)
        FILE = os.path.join(path, filename)
        if os.path.islink(FILE):
            continue

        #Taille du fichier
        size = os.path.getsize(FILE)

        md5 = md5sum(FILE)

        if md5 in MD5 :
            DOUBLON.append(FILE)
        else :
            MD5.append(md5)

for d in DOUBLON :
    print(d)

    #Déplacer dans dossier à supprimer
    shutil.move(d,r'C:\Users\Samantha.bizineche\Desktop\Test\Supprimer')